const child_process = require('child_process');
const child = child_process.fork('fib.js');

var k = 0;
child.send(k);
const MAX = process.argv[2];
const startTime = Date.now();

child.on('message', (msg) => {
	if(msg.event=="data"){
		console.log(`[${Date.now()-startTime}ms] Message from child #${msg.k}: ${msg.data}`);
		if(k<MAX){
			k = k+1;
			child.send(k);
		}
		else {
			child.disconnect();
		}
	} else {
		console.log("Unexpected message");
		process.exit(1);
	}
});

/*
 * Exercise: try to parallelise the computations by 
 *  spawning a configurable number of child "fib" processes.
 */