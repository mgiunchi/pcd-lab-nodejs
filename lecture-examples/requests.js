const request = require('request'), cheerio = require('cheerio');

if(process.argv.length < 3){ 
  console.log("USE: requests.js <URL>"); process.exit(0); 
}

const url = process.argv[2];
const MAX_REQUESTS = 3;
var k = 0;

function processLink(error, response, body) {
  if(response && response.statusCode==200){ 
  	$ = cheerio.load(body);
  	var links = $('a').map((i,x) => $(x).attr('href')).get();
  	k = k + 1;
  	console.log(`Processing link ${k}: ${response.request.uri.href}`+
  	  `\n${JSON.stringify(links,null,4)}\n\n`)
  	if(k < MAX_REQUESTS && links.length){
  	  request(links[Math.floor(Math.random()*links.length)], processLink);
  	} // Do you see any problems?
  }
}

request(url, processLink);