var sys = require("sys"), 
	http = require("http"), 
	url = require("url"),
	path = require("path"),
	fs = require("fs");

http.createServer(function(request, response) { 
	var uri = url.parse(request.url).pathname; 
	var filename = path.join(process.cwd(), uri); 
	fs.access(filename, function(err) { 
		if(!err) {
			fs.readFile(filename, function(err, data) { 
				response.writeHead(200); 
				response.end(data);
			}); 
		} else {
			response.writeHead(404); 
			response.end('NOT FOUND');
		}
	}); 
}).listen(8080); 

sys.log("Server running at http://localhost:8080/");